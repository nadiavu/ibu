require "rails_helper"

RSpec.describe Rate, :type => :model do
  describe '.find_rate' do
    let(:age) { 19 }
    let(:day_range) { 6 }
    it "returns the correct rate" do
      Rate.create(age_range: Range.new(18,49), day_range: Range.new(1,7), rate: 5000)

      expect(Rate.find_rate(age, day_range)).to eq(Money.new(5000))
    end
  end
end
