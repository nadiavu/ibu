require "rails_helper"

RSpec.describe AgeCalculator, :type => :module do
  describe '.calculate_age' do

    let(:birthday) { Date.parse("20/10/1994") }
    let(:fake_time) { Time.parse("07/02/2017") }

    it "calculates age" do
      allow(Time).to receive(:now).and_return(fake_time)
      expect(AgeCalculator.calculate(birthday)).to eq(22)
    end
  end
end
