require "rails_helper"

RSpec.feature "Rate", :type => :feature do
  scenario "User gets a rate" do
    Rate.create(age_range: Range.new(18,49), day_range: Range.new(1,7), rate: 5000)

    visit "/"

    select "1978", :from => "rate_age_1i"
    select "August", :from => "rate_age_2i"
    select "10", :from => "rate_age_3i"
    select "4", :from => "days"

    click_button "Get a quote"

    expect(page).to have_text("Fantastic, looks like your travel insurance will be 50.00 for your trip.")
  end
end
