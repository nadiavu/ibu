Rails.application.routes.draw do
  root 'rates#quote'
  get 'show', to: 'rates#show'
end
