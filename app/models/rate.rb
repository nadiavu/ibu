class Rate < ApplicationRecord
  monetize :rate, :as => "rate_money"

  def self.find_rate(age, days)
    Rate.where("age_range @> %i and day_range @> %i", age, days).first.rate_money
  end
end
