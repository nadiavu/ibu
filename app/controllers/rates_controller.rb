class RatesController < ApplicationController

  def quote
    render "quote"
  end

  def show
    rate = params["rate"]
    birthday = Date.new(rate["age(1i)"].to_i, rate["age(2i)"].to_i, rate["age(3i)"].to_i)
    age = AgeCalculator.calculate(birthday)
    @rate = Rate.find_rate(age, params[:days])
  end
end
