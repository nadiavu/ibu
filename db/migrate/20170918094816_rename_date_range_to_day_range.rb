class RenameDateRangeToDayRange < ActiveRecord::Migration[5.0]
  def change
    rename_column :rates, :date_range, :day_range
  end
end
