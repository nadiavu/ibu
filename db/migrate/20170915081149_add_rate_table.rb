class AddRateTable < ActiveRecord::Migration[5.0]
  def change
    create_table :rates do |t|
      t.int4range :age_range
      t.int4range :date_range
      t.integer :rate

      t.timestamps
    end
  end
end
