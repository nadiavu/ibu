# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
MAX_4INT = 2147483646

Rate.create(age_range: Range.new(18,49), day_range: Range.new(1,7), rate: 5000)
Rate.create(age_range: Range.new(50,59), day_range: Range.new(1,7), rate: 6000)
Rate.create(age_range: Range.new(60,69), day_range: Range.new(1,7), rate: 7000)

Rate.create(age_range: Range.new(18,49), day_range: Range.new(8,14), rate: 6000)
Rate.create(age_range: Range.new(50,59), day_range: Range.new(8,14), rate: 7300)
Rate.create(age_range: Range.new(60,69), day_range: Range.new(8,14), rate: 8000)

Rate.create(age_range: Range.new(18,49), day_range: Range.new(15,21), rate: 7000)
Rate.create(age_range: Range.new(50,59), day_range: Range.new(15,21), rate: 8000)
Rate.create(age_range: Range.new(60,69), day_range: Range.new(15,21), rate: 9000)

Rate.create(age_range: Range.new(18,49), day_range: Range.new(22, MAX_4INT), rate: 8200)
Rate.create(age_range: Range.new(50,59), day_range: Range.new(22, MAX_4INT), rate: 9000)
Rate.create(age_range: Range.new(60,69), day_range: Range.new(22, MAX_4INT), rate: 10000)
