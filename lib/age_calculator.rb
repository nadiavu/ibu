module AgeCalculator
  def self.calculate(birthday)
    now = Time.now.to_date
    age = now.year - birthday.year - ((now.month > birthday.month || (now.month == birthday.month && now.day >= birthday.day)) ? 0 : 1)
  end
end
